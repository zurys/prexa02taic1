// Seleccionar los elementos del DOM
const dogBreedSelect = document.getElementById('dog-breed-select');
const loadImageButton = document.getElementById('load-image');
const selectedImage = document.getElementById('selected-image');
const loadBreedsButton = document.getElementById('load-breeds');

// Cargar las razas de perros en el select cuando se haga clic en el botón
loadBreedsButton.addEventListener('click', () => {
    axios.get('https://dog.ceo/api/breeds/list')
        .then(response => {
            const breeds = response.data.message;

            // Agregar las opciones al select
            breeds.forEach(breed => {
                const option = document.createElement('option');
                option.value = breed;
                option.textContent = breed;
                dogBreedSelect.appendChild(option);
            });
        })
        .catch(error => {
            console.error('Error al cargar las razas de perros:', error);
        });
});

// Cargar una imagen aleatoria del perro seleccionado
loadImageButton.addEventListener('click', () => {
    const selectedBreed = dogBreedSelect.value;

    if (selectedBreed) {
        axios.get(`https://dog.ceo/api/breed/${selectedBreed}/images/random`)
            .then(response => {
                selectedImage.src = response.data.message;
            })
            .catch(error => {
                console.error('Error al cargar la imagen:', error);
            });
    }
});